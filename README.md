# Getting Started
## How to run
Run elastic search 
```bash
cd docker
docker-compose up
```

And Spring Boot:
```
mvn spring-boot:run
```

To execute cucumber scenarios:
``` 
mvn test
```

Actuator: http://localhost:8080/actuator/health
Kibana: http://localhost:5601/app/management/data/index_management/indices

## Documentation
Documentation of API here (Swagger-UI): http://localhost:8080/swagger-ui/index.html
I know about CRUD operations, but decided to make search with POST (because queries can be long, 
and it is easier to read it as json). Create/Update is combined into 1 save endpoint.
If id is present, then it will be updated, otherwise create.

Chosen ElasticSearch, because of fuzzy text search for instructions.

### Not Happy Flow scenarios
During save (create/update) of new recipe it passes validation, and if request is not correct user will get 400
and reason what exactly was wrong.
If anything goes wrong, user will get response like this 
HttpStatus: 500
```json
{
    "message": "Field numberOfPeople cannot be null"
}
```