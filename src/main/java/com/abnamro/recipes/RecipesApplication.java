package com.abnamro.recipes;

import com.abnamro.recipes.configurations.ElasticSearchProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(ElasticSearchProperties.class)
@SpringBootApplication
public class RecipesApplication {

  public static void main(String[] args) {
    SpringApplication.run(RecipesApplication.class, args);
  }

}
