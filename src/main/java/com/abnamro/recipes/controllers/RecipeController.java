package com.abnamro.recipes.controllers;

import com.abnamro.recipes.models.IdResponse;
import com.abnamro.recipes.models.Recipe;
import com.abnamro.recipes.models.SearchQuery;
import com.abnamro.recipes.services.RecipeService;
import com.abnamro.recipes.services.ValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RecipeController {
  private final RecipeService recipeService;
  private final ValidationService validationService;
  @PostMapping(value = "search", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Recipe> search(@RequestBody SearchQuery searchQuery) {
    return recipeService.search(searchQuery);
  }

  @PostMapping(value = "save", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public IdResponse save(@RequestBody Recipe recipe) {
    validationService.validateRecipe(recipe);
    return IdResponse.builder().id(recipeService.save(recipe)).build();
  }

  @DeleteMapping("delete/{id}")
  public void delete(@PathVariable String id) {
    recipeService.deleteById(id);
  }
}
