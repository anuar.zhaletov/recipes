package com.abnamro.recipes.exceptions;

public class ElasticSearchException extends RuntimeException {
  public ElasticSearchException(String message) {
    super(message);
  }
}
