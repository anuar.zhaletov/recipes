package com.abnamro.recipes.exceptions;

import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    String messageError;
}