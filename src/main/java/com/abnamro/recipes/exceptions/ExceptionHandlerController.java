package com.abnamro.recipes.exceptions;

import java.io.IOException;

import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlerController {

  @ExceptionHandler(ElasticSearchException.class)
  public ResponseEntity<ErrorResponse> handleCustomException(HttpServletResponse res, ElasticSearchException e) throws IOException {
    ErrorResponse errorResponse = new ErrorResponse(e.getLocalizedMessage());
    log.error("Exception happened. Cause: " + e.getLocalizedMessage(), e);
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<ErrorResponse> handleBadRequestException(HttpServletResponse res, BadRequestException e) throws IOException {
    ErrorResponse errorResponse = new ErrorResponse(e.getLocalizedMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  public ResponseEntity<ErrorResponse> handleHttpMediaTypeNotSupportedException(HttpServletResponse res, Exception e) throws IOException {
    ErrorResponse errorResponse = new ErrorResponse(e.getLocalizedMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleException(HttpServletResponse res, Exception e) throws IOException {
    ErrorResponse errorResponse = new ErrorResponse("Internal Server Error");
    log.error("Exception happened. Cause: " + e.getLocalizedMessage(), e);
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}

