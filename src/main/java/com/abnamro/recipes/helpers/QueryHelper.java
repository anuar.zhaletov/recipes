package com.abnamro.recipes.helpers;

import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.FuzzyQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class QueryHelper {
    public BoolQuery.Builder matchQueryString(BoolQuery.Builder boolQuery, String fieldName, String value) {
        if (value == null) {
            return boolQuery;
        }

        return boolQuery.must(MatchQuery.of(m -> m
                .field(fieldName)
                .query(value)
        )._toQuery());
    }

    public BoolQuery.Builder matchQueryBoolean(BoolQuery.Builder boolQuery, String fieldName, Boolean value) {
        if (value == null) {
            return boolQuery;
        }

        return boolQuery.must(MatchQuery.of(m -> m
                .field(fieldName)
                .query(value)
        )._toQuery());
    }

    public BoolQuery.Builder matchQueryInteger(BoolQuery.Builder boolQuery, String fieldName, Integer value) {
        if (value == null) {
            return boolQuery;
        }

        return boolQuery.must(MatchQuery.of(m -> m
                .field(fieldName)
                .query(value)
        )._toQuery());
    }

    public BoolQuery.Builder termsMustQuery(BoolQuery.Builder boolQuery, String fieldName, List<String> values) {
        if (values == null || values.isEmpty()) {
            return boolQuery;
        }

        return boolQuery.must(m -> m.terms(t -> t
            .field(fieldName)
            .terms(tt -> tt.value(values
                .stream()
                .map(FieldValue::of)
                .collect(Collectors.toList())))));
    }

    public BoolQuery.Builder termsMustNotQuery(BoolQuery.Builder boolQuery, String fieldName, List<String> values) {
        if (values == null || values.isEmpty()) {
            return boolQuery;
        }

        return boolQuery.mustNot(m -> m.terms(t -> t
            .field(fieldName)
            .terms(tt -> tt.value(values
                .stream()
                .map(FieldValue::of)
                .collect(Collectors.toList())))));
    }

    public BoolQuery.Builder fuzzyQuery(BoolQuery.Builder boolQuery, String fieldName, String value) {
        if (value == null) {
            return boolQuery;
        }

        return boolQuery.must(FuzzyQuery.of(m -> m
            .field(fieldName)
            .value(value)
            .fuzziness("AUTO")
        )._toQuery());
    }
}
