package com.abnamro.recipes.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Recipe {
  private String id;
  private String name;
  private Boolean vegeterian;
  private Integer numberOfPeople;
  private List<String> ingredients;
  private String instructions;
}
