package com.abnamro.recipes.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchQuery {
  private String name;
  private Boolean vegeterian;
  private Integer numberOfPeople;
  private List<String> mustIngredients;
  private List<String> mustNotIngredients;
  private String instructions;
}
