package com.abnamro.recipes.repositories;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import com.abnamro.recipes.helpers.QueryHelper;
import com.abnamro.recipes.models.Recipe;
import com.abnamro.recipes.models.SearchQuery;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class RecipeRepository {
  public static final String INDEX_NAME = "recipe";

  private final ElasticsearchClient elasticsearchClient;
  private final QueryHelper queryHelper;

  public List<Recipe> search(@NotNull SearchQuery searchQuery) throws IOException {
    SearchResponse<Recipe> searchResponse = elasticsearchClient.search(s -> s
            .index(INDEX_NAME)
            .query(q -> q
                .bool(b -> {
                      BoolQuery.Builder boolQuery = b;
                      boolQuery = queryHelper.matchQueryString(boolQuery, "name", searchQuery.getName());
                      boolQuery = queryHelper.matchQueryBoolean(boolQuery, "vegeterian", searchQuery.getVegeterian());
                      boolQuery = queryHelper.matchQueryInteger(boolQuery, "numberOfPeople", searchQuery.getNumberOfPeople());
                      boolQuery = queryHelper.termsMustQuery(boolQuery, "ingredients", searchQuery.getMustIngredients());
                      boolQuery = queryHelper.termsMustNotQuery(boolQuery, "ingredients", searchQuery.getMustNotIngredients());
                      boolQuery = queryHelper.fuzzyQuery(boolQuery, "instructions", searchQuery.getInstructions());
                      return boolQuery;
                    }
                )
            ),
        Recipe.class);

    List<Hit<Recipe>> hits = searchResponse.hits().hits();
    return hits.stream().map(Hit::source).collect(Collectors.toList());
  }

  public String save(Recipe recipe) throws IOException {
    String id = recipe.getId() != null ? recipe.getId() : UUID.randomUUID().toString();
    recipe.setId(id);
    elasticsearchClient.index(i -> i
        .index(INDEX_NAME)
        .id(id)
        .document(recipe));

    return id;
  }

  public void delete(@NotNull String id) throws IOException {
    elasticsearchClient.delete(i -> i.index(INDEX_NAME).id(id));
  }
}
