package com.abnamro.recipes.services;

import com.abnamro.recipes.exceptions.ElasticSearchException;
import com.abnamro.recipes.models.Recipe;
import com.abnamro.recipes.models.SearchQuery;
import com.abnamro.recipes.repositories.RecipeRepository;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class RecipeService {
  private final RecipeRepository recipeRepository;

  public List<Recipe> search(@NotNull SearchQuery searchQuery) {
    try {
      return recipeRepository.search(searchQuery);
    } catch (IOException e) {
      log.error("Failed to search recipe. Cause: " + e.getLocalizedMessage(), e);
      throw new ElasticSearchException("Failed to search recipe");
    }
  }

  public String save(@NotNull Recipe recipe) {
    try {
      return recipeRepository.save(recipe);
    } catch (IOException e) {
      log.error("Failed to save recipe. Cause: " + e.getLocalizedMessage(), e);
      throw new ElasticSearchException("Failed to save recipe");
    }
  }

  public void deleteById(@NotNull String id) {
    try {
      recipeRepository.delete(id);
    } catch (IOException e) {
      log.error("Failed to delete recipe. Cause: " + e.getLocalizedMessage(), e);
      throw new ElasticSearchException("Failed to delete recipe");
    }
  }
}
