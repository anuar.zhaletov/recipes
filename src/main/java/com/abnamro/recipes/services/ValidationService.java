package com.abnamro.recipes.services;

import com.abnamro.recipes.exceptions.BadRequestException;
import com.abnamro.recipes.models.Recipe;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {
  private static final int MAX_NAME_LENGTH = 50;
  private static final int MIN_NUMBER_OF_PEOPLE = 1;
  private static final int MAX_NUMBER_OF_PEOPLE = 50;

  public void validateRecipe(@NotNull Recipe recipe) {
    if (recipe.getVegeterian() == null) {
      throw new BadRequestException("Field vegeterian cannot be null");
    }
    if (recipe.getName() == null) {
      throw new BadRequestException("Field name cannot be null");
    }
    if (recipe.getName().length() > MAX_NAME_LENGTH) {
      throw new BadRequestException("Field name length is bigger then: " + MAX_NAME_LENGTH);
    }
    if (recipe.getNumberOfPeople() == null) {
      throw new BadRequestException("Field numberOfPeople cannot be null");
    }
    if (recipe.getNumberOfPeople() < MIN_NUMBER_OF_PEOPLE || recipe.getNumberOfPeople() > MAX_NUMBER_OF_PEOPLE) {
      throw new BadRequestException("Field numberOfPeople should be in range: " + MIN_NUMBER_OF_PEOPLE + " and "
          + MAX_NUMBER_OF_PEOPLE);
    }
    if (recipe.getIngredients() == null || recipe.getIngredients().isEmpty()) {
      throw new BadRequestException("Ingredients cannot be null");
    }
    if (recipe.getInstructions() == null || recipe.getInstructions().isEmpty()) {
      throw new BadRequestException("Instructions are empty");
    }
  }
}
