package com.abnamro;

import com.abnamro.recipes.exceptions.ErrorResponse;
import com.abnamro.recipes.models.Recipe;
import com.abnamro.recipes.models.SearchQuery;
import io.cucumber.java.en.*;
import io.restassured.response.Response;

import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.when;
import static io.restassured.RestAssured.with;
import static org.assertj.core.api.Assertions.assertThat;

public class StepDefinitions {
    private List<Recipe> result;
    private Response errorResponse;

    @Given("uploads {string}")
    public void userUploadsTomatoSoup(String recipeName) throws InterruptedException {
        Recipe recipe = null;
        if (recipeName.equals("tomato soup")) {
            recipe = Recipe.builder()
                    .id(UUID.randomUUID().toString())
                    .name(recipeName)
                    .vegeterian(true)
                    .numberOfPeople(4)
                    .ingredients(List.of("tomato", "garlic"))
                    .instructions("""
                            Saute Aromatics – heat a non-reactive pot over medium heat. Melt in 4 Tbsp butter then sautee onions until softened and golden (10-12 min). Add minced garlic and saute another minute.
                            Make the tomato soup base – stir in two 28 oz cans of crushed tomatoes with their juice, your chicken stock, chopped basil, sugar and black pepper. Bring to a boil then reduce heat, partially cover and simmer 10 minutes.
                            Blend if desired – use an immersion blender in the pot or blend in batches using a blender (be careful not to overfill the blender with hot liquid) and return soup to the pot.
                            Add cream and parmesan – stir in the heavy cream and shredded parmesan. Return to a simmer and season to taste if needed.
                            Serve – ladle into warm bowls and garnish with more parmesan and basil.
                            """)
                    .build();
        } else if (recipeName.equals("italian meringue")) {
            recipe = Recipe.builder()
                    .id(UUID.randomUUID().toString())
                    .name(recipeName)
                    .vegeterian(false)
                    .numberOfPeople(2)
                    .ingredients(List.of("rice", "egg", "sugar"))
                    .instructions("""
                            Put the sugar and water into a small saucepan over a medium heat with a thermometer resting in the liquid
                            Start to slowly whisk the egg whites either in a stand mixer or with a hand-held electric whisk until just starting to foam
                            Heat the sugar until it reaches 121°C then slowly pour into the egg whites whilst still whisking
                            Turn the whisk up to full speed and whisk until cooled to room temperature – the meringue is now ready to use
                            """)
                    .build();
        }

        if (recipe != null) {
            with()
                    .header("Content-Type", "application/json")
                    .body(recipe)
                    .when().post("http://localhost:8080/save")
                    .then()
                    .statusCode(200);
        }

        Thread.sleep(1000);
    }

    @Given("uploads recipe with missing fields")
    public void uploadsRecipeWithMissingFields() throws Exception {
        Recipe recipe = Recipe.builder()
                .id(UUID.randomUUID().toString())
                .name("NOT_VALID")
                .vegeterian(true)
                .numberOfPeople(4)
                .ingredients(List.of("tomato", "garlic"))
                // missing instructions
                .build();

        errorResponse = with()
                .header("Content-Type", "application/json")
                .body(recipe)
                .when().post("http://localhost:8080/save");

        Thread.sleep(1000);
    }

    @Then("error response is {string}")
    public void errorResponseIs(String errorMessage) throws Exception {
        errorResponse.then().statusCode(400);
        ErrorResponse eMessage = errorResponse.getBody().as(ErrorResponse.class);
        assertThat(eMessage.getMessageError()).isEqualTo(errorMessage);
        errorResponse = null;
    }

    @When("searches for recipe name starting with {string}")
    public void searchesByNameStaringWith(String prefixName) {
        SearchQuery searchQuery = SearchQuery.builder().name(prefixName).build();
        Response response = with()
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        result = List.of(response.getBody().as(Recipe[].class));
    }

    @Then("receives {int} recipe with name {string}")
    public void receives(int numberOfRecipes, String recipeName) {
        assertThat(result.size()).isEqualTo(numberOfRecipes);
        assertThat(result.get(0).getName()).isEqualTo(recipeName);
    }

    @When("searches vegeterian recipes")
    public void searchesVegetarian() {
        SearchQuery searchQuery = SearchQuery.builder().vegeterian(true).build();
        Response response = with()
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        result = List.of(response.getBody().as(Recipe[].class));
    }

    @When("search by numberOfPeople {int}")
    public void searchByNumberOfPeople(int numberOfPeople) {
        SearchQuery searchQuery = SearchQuery.builder().numberOfPeople(numberOfPeople).build();
        Response response = with()
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        result = List.of(response.getBody().as(Recipe[].class));
    }

    @When("search by non vegeterian and numberOfPeople {int} and ingredients")
    public void searchByVegeterianAndNumberOfPeople(int numberOfPeople, List<String> ingredients) {
        SearchQuery searchQuery = SearchQuery.builder().vegeterian(false).numberOfPeople(numberOfPeople)
                .mustIngredients(ingredients).build();
        Response response = with()
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        result = List.of(response.getBody().as(Recipe[].class));
    }

    @When("search by ingredients")
    public void searchByIngredients(List<String> ingredients) {
        SearchQuery searchQuery = SearchQuery.builder().mustIngredients(ingredients).build();
        Response response = with()
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        result = List.of(response.getBody().as(Recipe[].class));
    }

    @When("search by non matching ingredients")
    public void searchByNonMatchingIngredients(List<String> ingredients) {
        SearchQuery searchQuery = SearchQuery.builder().mustNotIngredients(ingredients).build();
        Response response = with()
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        result = List.of(response.getBody().as(Recipe[].class));
    }

    @When("search by instructions containing word {string}")
    public void searchByInstructions(String word) {
        SearchQuery searchQuery = SearchQuery.builder().instructions(word).build();
        Response response = with()
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        result = List.of(response.getBody().as(Recipe[].class));
    }

    @Then("clean up database")
    public void cleanUpDatabase() throws InterruptedException {
        Thread.sleep(1000);
        Response response = with()
                .body(SearchQuery.builder().build())
                .header("Content-Type", "application/json")
                .when().post("http://localhost:8080/search");

        List.of(response.getBody().as(Recipe[].class)).forEach(recipe ->
                when().delete("http://localhost:8080/delete/" + recipe.getId()).then().statusCode(200));
    }
}
