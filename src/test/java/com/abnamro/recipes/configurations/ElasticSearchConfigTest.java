package com.abnamro.recipes.configurations;


import co.elastic.clients.elasticsearch.ElasticsearchClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ElasticSearchConfigTest {
  private ElasticSearchConfig elasticSearchConfig;

  @BeforeEach
  void setUp() {
    this.elasticSearchConfig = new ElasticSearchConfig(ElasticSearchProperties.builder()
        .url("http://localhost:9200").build());
  }

  @Test
  void testElasticsearchClient() {
    ElasticsearchClient elasticsearchClient = elasticSearchConfig.elasticsearchClient();
    assertThat(elasticsearchClient).isNotNull();
  }
}