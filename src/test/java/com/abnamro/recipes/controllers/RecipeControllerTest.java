package com.abnamro.recipes.controllers;

import com.abnamro.recipes.models.IdResponse;
import com.abnamro.recipes.models.Recipe;
import com.abnamro.recipes.models.SearchQuery;
import com.abnamro.recipes.repositories.RecipeRepository;
import com.abnamro.recipes.services.RecipeService;
import com.abnamro.recipes.services.ValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class RecipeControllerTest {
  private static final List<Recipe> RECIPES = List.of(
      Recipe.builder()
          .id(UUID.randomUUID().toString())
          .name("TOMATO SOUP")
          .vegeterian(true)
          .numberOfPeople(4)
          .ingredients(List.of("tomato", "garlic"))
          .build()
  );

  private RecipeController recipeController;

  @Mock
  private ValidationService validationService;

  @Mock
  private RecipeService recipeService;

  @BeforeEach
  void setUp() {
    this.recipeController = new RecipeController(recipeService, validationService);
  }

  @Test
  void testSearch() {
    SearchQuery searchQuery = SearchQuery.builder()
        .name("Tomato Soup")
        .vegeterian(true)
        .numberOfPeople(4)
        .mustIngredients(List.of("tomato", "garlic"))
        .mustNotIngredients(List.of("meat"))
        .build();
    when(recipeService.search(eq(searchQuery))).thenReturn(RECIPES);
    List<Recipe> recipies = recipeController.search(searchQuery);
    assertThat(recipies).isEqualTo(RECIPES);
  }

  @Test
  void testSave() {
    String id = UUID.randomUUID().toString();
    Recipe recipe = Recipe.builder()
        .id(id)
        .name("TOMATO SOUP")
        .vegeterian(true)
        .numberOfPeople(4)
        .ingredients(List.of("tomato", "garlic"))
        .build();
    when(recipeService.save(eq(recipe))).thenReturn(id);
    IdResponse idResponse = recipeController.save(recipe);
    assertThat(idResponse.getId()).isEqualTo(id);
    verify(validationService).validateRecipe(eq(recipe));
  }

  @Test
  void testDelete() {
    String id = UUID.randomUUID().toString();
    recipeController.delete(id);
    verify(recipeService).deleteById(eq(id));
  }
}