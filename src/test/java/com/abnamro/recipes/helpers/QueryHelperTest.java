package com.abnamro.recipes.helpers;

import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class QueryHelperTest {
  private QueryHelper queryHelper;

  @BeforeEach
  void setUp() {
    this.queryHelper = new QueryHelper();
  }

  @Test
  void testMatchQueryStringWhenValueIsNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.matchQueryString(boolQueryBuilder, "name", null).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testMatchQueryStringWhenValueIsNotNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.matchQueryString(boolQueryBuilder, "name", "tomato").build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {\"must\":[{\"match\":{\"name\":{\"query\":\"tomato\"}}}]}");
  }

  @Test
  void testMatchQueryIntegerWhenValueIsNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.matchQueryInteger(boolQueryBuilder, "name", null).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testMatchQueryIntegerWhenValueIsNotNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.matchQueryInteger(boolQueryBuilder, "numberOfPeople", 4).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {\"must\":[{\"match\":{\"numberOfPeople\":{\"query\":4}}}]}");
  }

  @Test
  void testMatchQueryBooleanWhenValueIsNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.matchQueryBoolean(boolQueryBuilder, "name", null).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testMatchQueryBooleanWhenValueIsNotNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.matchQueryBoolean(boolQueryBuilder, "vegeterian", true).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {\"must\":[{\"match\":{\"vegeterian\":{\"query\":true}}}]}");
  }

  @Test
  void testTermsMustQueryWhenValueIsNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.termsMustQuery(boolQueryBuilder, "ingredients", null).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testTermsMustQueryWhenValueIsEmpty() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.termsMustQuery(boolQueryBuilder, "ingredients", List.of()).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testTermsMustQueryWhenValueIsNotEmpty() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.termsMustQuery(boolQueryBuilder, "ingredients", List.of("tomato")).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {\"must\":[{\"terms\":{\"ingredients\":[\"tomato\"]}}]}");
  }

  @Test
  void testTermsMustNotQueryWhenValueIsNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.termsMustNotQuery(boolQueryBuilder, "ingredients", null).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testTermsMustNotQueryWhenValueIsEmpty() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.termsMustNotQuery(boolQueryBuilder, "ingredients", List.of()).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testTermsMustNotQueryWhenValueIsNotEmpty() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.termsMustNotQuery(boolQueryBuilder, "ingredients", List.of("tomato")).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {\"must_not\":[{\"terms\":{\"ingredients\":[\"tomato\"]}}]}");
  }

  @Test
  void testFuzzyQueryWhenValueIsNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.fuzzyQuery(boolQueryBuilder, "instructions", null).build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {}");
  }

  @Test
  void testTermsMustNotQueryWhenValueIsNotNull() {
    BoolQuery.Builder boolQueryBuilder = new BoolQuery.Builder();
    BoolQuery boolQuery = queryHelper.fuzzyQuery(boolQueryBuilder, "instructions", "tomato").build();
    assertThat(boolQuery.toString()).isEqualTo("BoolQuery: {\"must\":[{\"fuzzy\":{\"instructions\":{\"fuzziness\":\"AUTO\",\"value\":\"tomato\"}}}]}");
  }
}