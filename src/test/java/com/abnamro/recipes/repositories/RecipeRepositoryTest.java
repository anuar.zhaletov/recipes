package com.abnamro.recipes.repositories;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch.core.DeleteRequest;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.util.ObjectBuilder;
import com.abnamro.recipes.helpers.QueryHelper;
import com.abnamro.recipes.models.Recipe;
import com.abnamro.recipes.models.SearchQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.function.Function;

import static org.mockito.ArgumentMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RecipeRepositoryTest {
  private RecipeRepository recipeRepository;
  @Mock
  private ElasticsearchClient elasticsearchClient;

  @Mock
  private QueryHelper queryHelper;

  @BeforeEach
  void setUp() {
    recipeRepository = new RecipeRepository(elasticsearchClient, queryHelper);
  }

  @Test
  void testDelete() throws Exception {
    String id = "ID";
    recipeRepository.delete(id);
    verify(elasticsearchClient)
        .delete(argThat((Function<DeleteRequest.Builder, ObjectBuilder<DeleteRequest>> fn) -> {
              DeleteRequest.Builder deleteRequestBuilder = new DeleteRequest.Builder();
              fn.apply(deleteRequestBuilder);
              DeleteRequest deleteRequest = deleteRequestBuilder.build();
              return deleteRequest.index().equals("recipe") && deleteRequest.id().equals(id);
            }
        ));
  }

  @Test
  void testSaveWithFilledRecipeId() throws Exception {
    Recipe recipe = Recipe.builder()
        .id("ID")
        .name("Tomato Soup")
        .vegeterian(true)
        .numberOfPeople(2)
        .ingredients(List.of("tomato", "garlic"))
        .instructions("Boil Tomato Soup")
        .build();
    recipeRepository.save(recipe);
    verify(elasticsearchClient)
        .index(argThat((Function<IndexRequest.Builder<Recipe>, ObjectBuilder<IndexRequest<Recipe>>> fn) -> {
              IndexRequest<Recipe> indexRecipe = IndexRequest.of(fn);
              Recipe recipeActual = indexRecipe.document();
              return recipeActual.equals(recipe);
            }
        ));
  }

  @Test
  void testSaveWithNullRecipeId() throws Exception {
    Recipe recipe = Recipe.builder()
        .id(null)
        .name("Tomato Soup")
        .vegeterian(true)
        .numberOfPeople(2)
        .ingredients(List.of("tomato", "garlic"))
        .instructions("Boil Tomato Soup")
        .build();

    recipeRepository.save(recipe);
    verify(elasticsearchClient)
        .index(argThat((Function<IndexRequest.Builder<Recipe>, ObjectBuilder<IndexRequest<Recipe>>> fn) -> {
              IndexRequest<Recipe> indexRecipe = IndexRequest.of(fn);
              Recipe recipeActual = indexRecipe.document();
              return recipeActual.getId() != null;
            }
        ));
  }

  @Test
  void testSearch() throws Exception {
    SearchQuery searchQuery = SearchQuery.builder()
        .name("Test Recipe")
        .vegeterian(true)
        .numberOfPeople(4)
        .mustIngredients(List.of("Ingredient1", "Ingredient2"))
        .mustNotIngredients(List.of("Ingredient3"))
        .build();

    SearchResponse<Recipe> mockedResponse = mock(SearchResponse.class);
    Hit<Recipe> hit1 = mock(Hit.class);
    Hit<Recipe> hit2 = mock(Hit.class);
    List<Hit<Recipe>> hits = List.of(hit1, hit2);
    when(mockedResponse.hits()).thenReturn(mock(HitsMetadata.class));
    when(mockedResponse.hits().hits()).thenReturn(hits);

    when(elasticsearchClient.search(any(Function.class), eq(Recipe.class))).thenReturn(mockedResponse);

    List<Recipe> results = recipeRepository.search(searchQuery);

    assertThat(results).hasSize(2);
  }
}