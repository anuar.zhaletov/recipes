package com.abnamro.recipes.services;

import com.abnamro.recipes.exceptions.ElasticSearchException;
import com.abnamro.recipes.models.IdResponse;
import com.abnamro.recipes.models.Recipe;
import com.abnamro.recipes.models.SearchQuery;
import com.abnamro.recipes.repositories.RecipeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecipeServiceTest {
  private static final SearchQuery SEARCH_QUERY = SearchQuery.builder()
      .name("Tomato Soup")
      .vegeterian(true)
      .numberOfPeople(4)
      .mustIngredients(List.of("tomato", "garlic"))
      .mustNotIngredients(List.of("meat"))
      .build();

  private static final List<Recipe> RECIPES = List.of(
      Recipe.builder()
          .id(UUID.randomUUID().toString())
          .name("TOMATO SOUP")
          .vegeterian(true)
          .numberOfPeople(4)
          .ingredients(List.of("tomato", "garlic"))
          .build()
  );

  private RecipeService recipeService;

  @Mock
  private RecipeRepository recipeRepository;

  @BeforeEach
  void setUp() {
    this.recipeService = new RecipeService(recipeRepository);
  }

  @Test
  void searchNotHappyFlow() throws Exception {
    when(recipeRepository.search(SEARCH_QUERY)).thenThrow(IOException.class);
    Exception exception = assertThrows(ElasticSearchException.class, () -> recipeService.search(SEARCH_QUERY));
    assertThat(exception.getMessage()).isEqualTo("Failed to search recipe");
  }

  @Test
  void searchHappyFlow() throws Exception {
    when(recipeRepository.search(SEARCH_QUERY)).thenReturn(RECIPES);
    assertThat(recipeService.search(SEARCH_QUERY)).isEqualTo(RECIPES);
  }

  @Test
  void saveNotHappyFlow() throws Exception {
    when(recipeRepository.save(RECIPES.get(0))).thenThrow(IOException.class);
    Exception exception = assertThrows(ElasticSearchException.class, () -> recipeService.save(RECIPES.get(0)));
    assertThat(exception.getMessage()).isEqualTo("Failed to save recipe");
  }

  @Test
  void saveHappyFlow() throws Exception {
    when(recipeRepository.save(RECIPES.get(0))).thenReturn("SOME_ID");
    assertThat(recipeService.save(RECIPES.get(0))).isEqualTo("SOME_ID");
  }

  @Test
  void deleteByIdNotHappyFlow() throws Exception {
    doThrow(IOException.class).when(recipeRepository).delete("SOME_ID");
    Exception exception = assertThrows(ElasticSearchException.class, () -> recipeService.deleteById("SOME_ID"));
    assertThat(exception.getMessage()).isEqualTo("Failed to delete recipe");
  }

  @Test
  void deleteByIdHappyFlow() throws Exception {
    recipeService.deleteById("SOME_ID");
    verify(recipeRepository).delete(eq("SOME_ID"));
  }
}