package com.abnamro.recipes.services;

import com.abnamro.recipes.exceptions.BadRequestException;
import com.abnamro.recipes.models.Recipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationServiceTest {
  private ValidationService validationService;

  @BeforeEach
  void setUp() {
    this.validationService = new ValidationService();
  }

  @Test
  void testShouldThrowExceptionWhenVegeterianIsNull() {
    Recipe recipe = Recipe.builder().build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validateRecipe(recipe));
    assertThat(exception.getMessage()).isEqualTo("Field vegeterian cannot be null");
  }

  @Test
  void testShouldThrowExceptionWhenNameIsNull() {
    Recipe recipe = Recipe.builder().vegeterian(true).build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validateRecipe(recipe));
    assertThat(exception.getMessage()).isEqualTo("Field name cannot be null");
  }

  @Test
  void testShouldThrowExceptionWhenNameLengthBiggerThenMax() {
    Recipe recipe = Recipe.builder().vegeterian(true).name("""
        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
        """).build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validateRecipe(recipe));
    assertThat(exception.getMessage()).isEqualTo("Field name length is bigger then: 50");
  }

  @Test
  void testShouldThrowExceptionWhenNumberOfPeopleIsNull() {
    Recipe recipe = Recipe.builder().vegeterian(true).name("Tomato Soup").build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validateRecipe(recipe));
    assertThat(exception.getMessage()).isEqualTo("Field numberOfPeople cannot be null");
  }

  @Test
  void testShouldThrowExceptionWhenNumberOfPeopleBiggerThenMax() {
    Recipe recipe = Recipe.builder().vegeterian(true).name("Tomato Soup").numberOfPeople(100).build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validateRecipe(recipe));
    assertThat(exception.getMessage()).isEqualTo("Field numberOfPeople should be in range: 1 and 50");
  }

  @Test
  void testShouldThrowExceptionWhenIngredientsAreEmpty() {
    Recipe recipe = Recipe.builder().vegeterian(true).name("Tomato Soup").numberOfPeople(3).build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validateRecipe(recipe));
    assertThat(exception.getMessage()).isEqualTo("Ingredients cannot be null");
  }

  @Test
  void testShouldThrowExceptionWhenInstructionsAreEmpty() {
    Recipe recipe = Recipe.builder().vegeterian(true).name("Tomato Soup").ingredients(List.of("Garlic", "Tomato"))
        .numberOfPeople(3).build();
    Exception exception = assertThrows(BadRequestException.class, () -> validationService.validateRecipe(recipe));
    assertThat(exception.getMessage()).isEqualTo("Instructions are empty");
  }

  @Test
  void testPassHappyFlow() {
    Recipe recipe = Recipe.builder().vegeterian(true).name("Tomato Soup").numberOfPeople(3)
        .ingredients(List.of("Garlic", "Tomato")).instructions("Some instructions").build();
    validationService.validateRecipe(recipe);
  }
}