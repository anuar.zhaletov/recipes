Feature: An example

  Scenario: User uploads recipe (create index)
    When uploads "tomato soup"

  Scenario: User is searching by name
    Given clean up database
    When uploads "tomato soup"
    When uploads "italian meringue"
    When searches for recipe name starting with "tomato soup"
    Then receives 1 recipe with name "tomato soup"

  Scenario: User is searching vegeterian recipes
    Given clean up database
    When uploads "tomato soup"
    When uploads "italian meringue"
    When searches vegeterian recipes
    Then receives 1 recipe with name "tomato soup"

  Scenario: User is searching by numberOfPeople
    Given clean up database
    When uploads "tomato soup"
    When uploads "italian meringue"
    When search by numberOfPeople 2
    Then receives 1 recipe with name "italian meringue"

  Scenario: User is searching by vegeterian and numberOfPeople and ingredients
    Given clean up database
    When uploads "tomato soup"
    When uploads "italian meringue"
    When search by non vegeterian and numberOfPeople 2 and ingredients
      | egg |
    Then receives 1 recipe with name "italian meringue"

  Scenario: User is searching by instructions contains the word
    Given clean up database
    When uploads "tomato soup"
    When uploads "italian meringue"
    When search by instructions containing word "basil"
    Then receives 1 recipe with name "tomato soup"

  Scenario: User is uploading not valid recipe
    Given clean up database
    When uploads recipe with missing fields
    Then error response is "Instructions are empty"